from django.conf.urls import url
from django.views.generic import TemplateView

from .api import ListApi, CardApi
from .views import TestList

urlpatterns = [
    url(r'^lists$', ListApi.as_view()),
    url(r'^cards$', CardApi.as_view()),
    url(r'^home', TemplateView.as_view(template_name="scrumboard/home.html")),
    url(r'^poke/', TestList.as_view()),
]