from rest_framework import serializers

from .models import List, Card

#serializar sirve para que cuando mandemos los datos a angular los mande serializados en JSON

class CardSerializer(serializers.ModelSerializer):

    class Meta:
        model = Card
        fields = ('id', 'title','description','list','story_points','business_value')


class ListSerializer(serializers.ModelSerializer):
    cards = CardSerializer(read_only=True, many=True)

    class Meta:
        model = List
        fields = ('id','name','cards')
        #fields = '__all__'



